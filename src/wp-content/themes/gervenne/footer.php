   <a href="javascript:;" id="scrollTop">
      <i class="fa fa-chevron-circle-up" aria-hidden="true"></i>
   </a>

   <div id="modal_vid">
      <div class="layer-mask"></div>
      <span class="close-btn">x</span>
      <div class="vid-wrap">
         <div class="embed-responsive embed-responsive-16by9">
            <iframe id="vid_detail" src="" frameborder="0" allowfullscreen></iframe>
         </div>
      </div>
   </div>

   <footer id="footer">
      <div class="content-wrap">
         <a class="logo" href="<?php home_url();?>">
            <img src="<?php bloginfo('template_directory');?>/images/logo.png" alt="">
         </a>
         <div class="content">
            <?php the_field('address', 'option'); ?><br><br>
            <i>Mọi thắc mắc xin liên hệ: <a href="tel:<?php the_field('phone_number', 'option'); ?>"><?php the_field('phone_number', 'option'); ?></a></i>
         </div>
         <ul class="socials">
            <li>
               <a href="<?php the_field('link_youtube', 'option'); ?>" target="_blank">
                  <i class="fa fa-youtube" aria-hidden="true"></i>
               </a>
            </li>
            <li>
               <a href="<?php the_field('link_facebook', 'option'); ?>" target="_blank">
                  <i class="fa fa-facebook" aria-hidden="true"></i>
               </a>
            </li>
         </ul>
      </div>
   </footer>

   <!-- Load Facebook SDK for JavaScript -->
   <!-- Your customer chat code -->
   <div id="fb-root"></div>
   <script>
      window.fbAsyncInit = function() {
         FB.init({
            xfbml            : true,
            version          : 'v3.2'
         });
      };
      (function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = 'https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js';
      fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));
   </script>

   <div class="fb-customerchat"
      attribution=setup_tool
      page_id="688168121393195"
      logged_in_greeting="Xin chào! Chúng tôi có thể giúp gì cho bạn?"
      logged_out_greeting="Xin chào! Chúng tôi có thể giúp gì cho bạn?">
   </div>

   <script type="text/javascript" src="<?php bloginfo('template_directory');?>/js/bootstrap.js"></script>
   <script type="text/javascript" src="<?php bloginfo('template_directory');?>/js/TweenMax.min.js"></script>
   <script type="text/javascript" src="<?php bloginfo('template_directory');?>/js/ScrollMagic.min.js"></script>
   <script type="text/javascript" src="<?php bloginfo('template_directory');?>/js/animation.gsap.min.js"></script>
   <script type="text/javascript" src="<?php bloginfo('template_directory');?>/js/slick.js"></script>
   <script type="text/javascript" src="<?php bloginfo('template_directory');?>/js/main.js"></script>
   <!-- scripts -->

   <?php wp_footer();?>
</body>
</html>