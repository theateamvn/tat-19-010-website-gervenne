<!DOCTYPE html>
<html class="no-js" lang="en">

<head>
   <!-- Locale -->
   <meta http-equiv="Content-Language" content="en">

   <!-- To the Future! -->
   <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

   <!-- Meta -->
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
   <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
   <meta name="generator" content="Gervenne">
   <meta http-equiv="cache-control" content="no-cache" />
   <meta http-equiv="Pragma" content="no-cache" />
   <meta http-equiv="Expires" content="-1" />

   <meta property="og:url" content="<?php home_url();?>" />
   <meta property="og:type" content="website" />
   <meta property="og:title" content="GERVENNE" />
   <meta property="og:description" content="Trắng Thơm Khó Giấu" />
   <meta property="og:image" content="<?php bloginfo('template_directory');?>/images/home/banner-1.jpg" />

   <title>GERVENNE</title>
   <meta name="description" content="GERVENNE" />

   <!-- Favicons -->
   <link rel="icon" type="image/x-icon" href="<?php bloginfo('template_directory');?>/images/favicon.ico">
   <link rel="shortcut icon" type="image/x-icon" href="<?php bloginfo('template_directory');?>/images/favicon.ico">
   <link rel="apple-touch-icon" href="<?php bloginfo('template_directory');?>/images/favicon.ico">
   <!-- CSS -->
   <link href="https://fonts.googleapis.com/css?family=Play:400,700&amp;subset=vietnamese" rel="stylesheet">
   <link rel="stylesheet" href="<?php bloginfo('template_directory');?>/css/styles.css" />

   <script type="text/javascript" src="<?php bloginfo('template_directory');?>/js/jquery.min.js"></script>
   <?php the_field('google_analytic', 'option'); ?>

</head>
<body>
<?php the_field('google_tag_manager_no_script', 'option'); ?>

<?php
   $current_page = 'home';
   if($wp->request){
   $current_page =$wp->request;
   }
?>
<header id="header">
   <div class="content-wrap">
      <a class="logo" href="<?php home_url();?>/">
         <img src="<?php bloginfo('template_directory');?>/images/logo.png" alt="">
      </a>
      <a class="nav-control" href="javascript:;">
         <span></span>
      </a>
      <ul class="main-nav">
         <li>
            <a href="<?php home_url();?>/" data-href="#banner">Trang chủ</a>
         </li>
         <li>
            <a href="<?php home_url();?>/#product" data-href="#product">sản phẩm</a>
         </li>
         <li>
            <a href="<?php home_url();?>/#brand" data-href="#brand">khám phá thương hiệu</a>
         </li>
         <li>
            <a href="javascript:;">khuyến mãi</a>
         </li>
         <li>
            <a href="<?php home_url();?>/phan-phoi">PHÂN PHỐI</a>
         </li>
         <li class="socials">
            <a href="<?php the_field('link_youtube', 'option'); ?>" target="_blank">
               <i class="fa fa-youtube" aria-hidden="true"></i>
            </a>
            <a href="<?php the_field('link_facebook', 'option'); ?>" target="_blank">
               <i class="fa fa-facebook" aria-hidden="true"></i>
            </a>
            <a href="<?php the_field('link_shop_online', 'option'); ?>" target="_blank">
               <i class="fa fa-shopping-bag" aria-hidden="true"></i>
            </a>
         </li>
      </ul>
   </div>
</header>