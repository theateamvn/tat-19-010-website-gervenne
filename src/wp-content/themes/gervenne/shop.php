<?php
/**
 *Template Name: Shop
 */
get_header();
?>
<!-- main -->
<div id="bubbles">
   <div class="dealer-bubble">
      <img class="bubble left-1" src="<?php bloginfo('template_directory');?>/images/dealer/icon-b3.png" alt="">
      <img class="bubble left-2" src="<?php bloginfo('template_directory');?>/images/dealer/icon-flower-2.png" alt="">
      <img class="bubble left-3" src="<?php bloginfo('template_directory');?>/images/dealer/icon-flower-2.png" alt="">
      <img class="bubble left-4" src="<?php bloginfo('template_directory');?>/images/dealer/icon-bubble-1.png" alt="">
      <img class="bubble right-1" src="<?php bloginfo('template_directory');?>/images/dealer/icon-bubble-2.png" alt="">
      <img class="bubble right-2" src="<?php bloginfo('template_directory');?>/images/dealer/icon-bubble-1.png" alt="">
      <img class="bubble right-3" src="<?php bloginfo('template_directory');?>/images/dealer/icon-flower-1.png" alt="">
      <img class="bubble right-4" src="<?php bloginfo('template_directory');?>/images/dealer/icon-bubble-1.png" alt="">
      <img class="bubble right-5" src="<?php bloginfo('template_directory');?>/images/dealer/icon-b3.png" alt="">
   </div>
</div>

<section id="dealer">
   <div class="list-wrap">
      <?php 
		$post_type = 'shop-phan-phoi';
		$taxonomy = 'he-thong-phan-phoi';
		$get_terms_args = array();
		$wp_query_args = array();
		$tax_terms = get_terms( $taxonomy, $get_terms_args );
		if( $tax_terms ){
		foreach( $tax_terms as $tax_term ){
		$query_args = array(
            'post_type' => $post_type,
            "$taxonomy" => $tax_term->slug,
            'post_status' => 'publish',
            'posts_per_page' => -1,
            'ignore_sticky_posts' => true,
            'orderby'   => 'meta_value',
            'order' => 'ASC',
            );
		$query_args = wp_parse_args( $wp_query_args, $query_args );
	   $my_query = new WP_Query( $query_args );
		if( $my_query->have_posts() && $tax_term->slug) { ?>
      <div class="list-content">
         <h2 class="title"><?php echo $tax_term->name; ?></h2>
         <?php
         $class_type = "market";
         if($tax_term->slug == 'he-thong-online'){
            $class_type = "online";
         }
         ?>
         <ul class="<?php echo $class_type;?>-list">
         <?php while ($my_query->have_posts()) : $my_query->the_post(); ?>
            <li>
               <?php if($class_type == "market") {?>
                     <img src="<?php echo get_post_meta( $post->ID, 'wpcf-shop_hinh', true ); ?>" alt="<?php echo get_the_title();?>">
               <?php } else {?>
                  <a href="<?php echo get_post_meta( $post->ID, 'wpcf-shop_link', true ); ?>" target="_blank">
                     <img src="<?php echo get_post_meta( $post->ID, 'wpcf-shop_hinh', true ); ?>" alt="<?php echo get_the_title();?>">
                  </a>
               <?php }?>
            </li>
         <?php endwhile; ?>
         </ul>
      </div>
      <?php
		}
		wp_reset_query();
		}
		}
      ?>
      <div class="list-content">
         <h2 class="title">KÊNH TRUYỀN THỐNG</h2>
         <h2 class="txt-store">Hệ thống các cửa hàng tạp hoá và mỹ phẩm trên toàn quốc</h2>
      </div>
   </div>
</section>
<!-- end main -->
	<script type="text/javascript" src="<?php bloginfo('template_directory');?>/js/dealer.js"></script>
<?php
get_footer();
