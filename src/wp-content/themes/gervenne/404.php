<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>

	<div id="notfound_404">
		<div class="content_404">
			<div class="text_404">
				<h1>404</h1>
				<h2>Không tìm thấy trang</h2>
			</div>
			<p class="cd_404">
				Trở về trang chủ sau <span>9</span> giây
			</p>
		</div>
	</div>

   <script type="text/javascript" src="<?php bloginfo('template_directory');?>/js/404.js"></script>
<?php get_footer(); ?>
