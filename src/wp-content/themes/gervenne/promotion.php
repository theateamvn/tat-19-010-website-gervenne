<?php 
/**
 *Template Name: Promotion
 */
get_header();?>

<!-- main -->
<div id="bubbles" class="bubbles-promotion">
   <div class="home-bubble">
      <img class="bubble left-1" src="<?php bloginfo('template_directory');?>/images/home/icon-b3-1.png" alt="">
      <img class="bubble left-2" src="<?php bloginfo('template_directory');?>/images/home/icon-flower-2.png" alt="">
      <img class="bubble left-3" src="<?php bloginfo('template_directory');?>/images/home/icon-flower-2.png" alt="">
      <img class="bubble left-4" src="<?php bloginfo('template_directory');?>/images/home/icon-bubble-1.png" alt="">

      <img class="bubble left-6" src="<?php bloginfo('template_directory');?>/images/home/icon-bubble-3.png" alt="">
      <img class="bubble left-7" src="<?php bloginfo('template_directory');?>/images/home/icon-bubble-3.png" alt="">
      <img class="bubble left-8" src="<?php bloginfo('template_directory');?>/images/home/icon-bubble-3.png" alt="">
      <img class="bubble right-3" src="<?php bloginfo('template_directory');?>/images/home/icon-flower-1.png" alt="">
      <img class="bubble right-4" src="<?php bloginfo('template_directory');?>/images/home/icon-flower-4.png" alt="">
      <img class="bubble right-6" src="<?php bloginfo('template_directory');?>/images/home/icon-b3-1.png" alt="">
      <img class="bubble right-8" src="<?php bloginfo('template_directory');?>/images/home/icon-flower-3.png" alt="">
      <img class="bubble right-18" src="<?php bloginfo('template_directory');?>/images/home/icon-b3-2.png" alt="">
      <img class="bubble right-20" src="<?php bloginfo('template_directory');?>/images/home/icon-bubble-5.png" alt="">
   </div>
</div>
<?php 
   $post_type = 'promotion';
   $taxonomy = 'promotion-catergory';
   $get_terms_args = array(
      'orderby' => 'slug'
   );
   $wp_query_args = array();
   $tax_terms = get_terms( $taxonomy, $get_terms_args );
   if( $tax_terms ){
   ?>
<section id="promotion" class="promotion-wrapper">
   <div class="nav-wrap">
      <ul class="cat-nav"> 
         <?php
         foreach( $tax_terms as $key => $tax_term ){
            $query_args = array(
                  'post_type' => $post_type,
                  "$taxonomy" => $tax_term->slug,
                  'post_status' => 'publish',
                  'posts_per_page' => -1,
                  'ignore_sticky_posts' => true
                  );
            $query_args = wp_parse_args( $wp_query_args, $query_args );
            $my_query = new WP_Query( $query_args );
         if( $my_query->have_posts() && $tax_term->slug) { 
            if($tax_term->slug == 'chuong-trinh-khuyen-mai') {?>
            <li>
               <a href="/khuyen-mai/chuong-trinh-khuyen-mai" data-href="#cat_<?php echo $tax_term->slug; ?>"><?php echo $tax_term->name; ?></a>
            </li>
         <?php }else { ?>
            <li>
               <a class="<?php if(0 == $key) echo 'active' ?>" href="javascript:;" data-href="#cat_<?php echo $tax_term->slug; ?>"><?php echo $tax_term->name; ?></a>
            </li>
         <?php }} }?>
      </ul>
   </div>
   <div class="content-wrap">
      <div class="cat-list">
      <?php
      foreach( $tax_terms as $key => $tax_term ){
         $query_args = array(
            'post_type' => $post_type,
            "$taxonomy" => $tax_term->slug,
            'post_status' => 'publish',
            'posts_per_page' => -1,
            'ignore_sticky_posts' => true
            );
         $query_args = wp_parse_args( $wp_query_args, $query_args );
         $my_query = new WP_Query( $query_args );
         if( $my_query->have_posts() && $tax_term->slug) {
            if($tax_term->slug != 'chuong-trinh-khuyen-mai') {
            ?>
         <div id="cat_<?php echo $tax_term->slug; ?>" class="<?php if(0 == $key) echo 'active';?> cat-item">
            <div class="item-list">
               <h2 class="title"><?php echo get_term_meta( $tax_term->term_taxonomy_id, 'wpcf-promotions-category-title', true ); ?></h2>
               <p class="desc"><?php echo $tax_term->description; ?></p>
               <ul id="list_slide" class="list-slide list-slide-promotion">
                  <?php while ($my_query->have_posts()) : $my_query->the_post(); ?>
                     <li>
                        <img class="img-product" src="<?php echo get_post_meta( $post->ID, 'wpcf-promotion_hinh-khuyen-mai', true ); ?>" alt="">
                        <div class="img-name">
                        <?php echo get_post_meta( $post->ID, 'wpcf-promotion_ten-san-pham-khuyen-mai', true ); ?>
                        <br/>
                        <span><?php echo the_title(); ?></span>
                        </div>
                        <a href="<?php echo get_post_meta( $post->ID, 'wpcf-link-mua-ngay', true ); ?>" target="_blank" data-index="<?php echo $my_query->current_post ?>" class="view-btn">
                           <img src="<?php bloginfo('template_directory');?>/images/promotion/btn-km-muangay.png" alt="">
                        </a>
                     </li>
                  <?php endwhile; ?>
               </ul>
            </div>
         </div>
      <?php 
         }
      }}?>
      </div>
   </div>
   <?php
      wp_reset_query();
      }
      ?>
</section>
<!-- end main -->
   <script type="text/javascript" src="<?php bloginfo('template_directory');?>/js/promotion.js"></script>
<?php get_footer();?>