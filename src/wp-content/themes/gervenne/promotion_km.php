<?php 
/**
 *Template Name: Promotion KM
 */
get_header();?>

<!-- main -->
<?php 
   $post_type = 'promotion';
   $taxonomy = 'promotion-catergory';
   $get_terms_args = array(
      'orderby' => 'slug'
   );
   $wp_query_args = array();
   $tax_terms = get_terms( $taxonomy, $get_terms_args );
   if( $tax_terms ){
   ?>
<section id="promotion_km" class="promotion-wrapper">
   <div class="nav-wrap">
      <ul class="cat-nav"> 
         <?php
         foreach( $tax_terms as $key => $tax_term ){
            $query_args = array(
                  'post_type' => $post_type,
                  "$taxonomy" => $tax_term->slug,
                  'post_status' => 'publish',
                  'posts_per_page' => -1,
                  'ignore_sticky_posts' => true
                  );
            $query_args = wp_parse_args( $wp_query_args, $query_args );
            $my_query = new WP_Query( $query_args );
         if( $my_query->have_posts() && $tax_term->slug) { 
            if($tax_term->slug == 'chuong-trinh-khuyen-mai') {?>
            <li>
               <a class="active" href="javascript:;" data-href="#cat_<?php echo $tax_term->slug; ?>"><?php echo $tax_term->name; ?></a>
            </li>
         <?php
         } else {?>
            <li>
               <a class="" href="<?php home_url();?>/khuyen-mai/<?php echo get_term_meta( $tax_term->term_taxonomy_id, 'wpcf-promotions-category-slug', true ); ?>" data-href="#cat_<?php echo $tax_term->slug; ?>"><?php echo $tax_term->name; ?></a>
            </li>
         <?php } } }?>
      </ul>
   </div>
   <div class="content-promotion-wrap">
      <div class="cat-list">
      <?php
      foreach( $tax_terms as $key => $tax_term ){
         $query_args = array(
            'post_type' => $post_type,
            "$taxonomy" => $tax_term->slug,
            'post_status' => 'publish',
            'posts_per_page' => -1,
            'ignore_sticky_posts' => true
            );
         $query_args = wp_parse_args( $wp_query_args, $query_args );
         $my_query = new WP_Query( $query_args );
         if( $my_query->have_posts() && $tax_term->slug) {
            if($tax_term->slug == 'chuong-trinh-khuyen-mai') {
            ?>
            <div id="cat_<?php echo $tax_term->slug; ?>" class="active cat-item">
               <div class="item-list">
                  <ul id="list_slide_promotion" class="list-slide list-promotion">
                  <?php while ($my_query->have_posts()) : $my_query->the_post(); ?>
                     <li>
                        <img class="img-product" src="<?php echo get_post_meta( $post->ID, 'wpcf-promotion_hinh-khuyen-mai', true ); ?>" alt="">
                     </li>
                  <?php endwhile; ?>
                  </ul>
               </div>
            </div>
            <?php
         }
      }}?>
      </div>
   </div>
   <?php
      wp_reset_query();
      }
      ?>
</section>
<!-- end main -->
   <script type="text/javascript" src="<?php bloginfo('template_directory');?>/js/promotion_km.js"></script>
<?php get_footer();?>