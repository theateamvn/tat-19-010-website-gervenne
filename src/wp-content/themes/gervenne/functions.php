<?php


/*
* Hide admin menu
*/
function hide_admin_menus() {
//     //remove_menu_page( 'index.php' ); // Menu Bảng tin
    remove_menu_page( 'edit.php' ); // Menu Bài viết
    remove_menu_page( 'upload.php' ); // Menu Media
    //remove_menu_page( 'edit.php?post_type=page' ); // Menu Trang
    remove_menu_page( 'edit-comments.php' ); // Menu Bình luận
    remove_menu_page( 'themes.php' ); // Menu Giao diện
    //remove_menu_page( 'plugins.php' ); // Menu Plugins
    remove_menu_page( 'users.php' ); // Menu Thành viên
    remove_menu_page( 'tools.php' ); // Menu Công cụ
   // remove_menu_page( 'options-general.php' );
    remove_menu_page( 'edit.php?post_type=acf-field-group' );
}
add_action( 'admin_menu', 'hide_admin_menus' );


if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
		'page_title' 	=> 'Theme options', // Title hiển thị khi truy cập vào Options page
		'menu_title'	=> 'Theme options', // Tên menu hiển thị ở khu vực admin
		'menu_slug' 	=> 'theme-settings', // Url hiển thị trên đường dẫn của options page
		'capability'	=> 'edit_posts',
		'redirect'	=> false
    ));
}
// remove unwanted dashboard widgets for relevant users
function wptutsplus_remove_dashboard_widgets() {
        remove_meta_box( 'dashboard_activity', 'dashboard', 'normal' );
        remove_meta_box( 'wptutsplus_dashboard_links', 'dashboard', 'normal' );
        remove_meta_box( 'dashboard_right_now', 'dashboard', 'normal' );
        remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
        remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
        remove_meta_box( 'dashboard_primary', 'dashboard', 'side' );
        remove_meta_box( 'dashboard_secondary', 'dashboard', 'side' );
}
add_action( 'wp_dashboard_setup', 'wptutsplus_remove_dashboard_widgets' );


// adđ new box dashboard
function wptutsplus_add_dashboard_widgets() {
    wp_add_dashboard_widget( 'wptutsplus_dashboard_welcome', 'Welcome', 'wptutsplus_add_welcome_widget' );
}
function wptutsplus_add_welcome_widget(){ ?>
<div id="welcome-panel" class="welcome-panel">
    <h2>Các Chức Năng Trong BackEnd</h2>
    <div class="welcome-panel-column-container">
    <ul>
        <li><strong>Trang Chủ</strong>
            <ul style="padding-left: 20px;">
                <li><a href="admin.php?page=san-pham">Sản Phẩm</a></li>
                <li><a href="admin.php?page=theme-settings">Truyền Thông</a>: mục Video Truyền Thông</li>
            </ul>
        </li>
        <li><strong>Trang Khuyến Mãi</strong>: <a href="edit.php?post_type=promotion">Link</a>
        </li>
        <li><strong>Trang Phân Phối</strong>: <a href="edit.php?post_type=phan-phoi">Link</a>
        </li>
        <li><strong>Thay Đổi Các Link Social</strong>: <a href="admin.php?page=theme-settings">Link</a>
        </li>
    </ul>
    </div>
</div>
<?php }
add_action( 'wp_dashboard_setup', 'wptutsplus_add_dashboard_widgets' );
?>