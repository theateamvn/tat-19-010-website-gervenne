<?php get_header();?>

<!-- main -->
<div id="bubbles">
   <div class="home-bubble">
      <img class="bubble left-1" src="<?php bloginfo('template_directory');?>/images/home/icon-b3-1.png" alt="">
      <img class="bubble left-2" src="<?php bloginfo('template_directory');?>/images/home/icon-flower-2.png" alt="">
      <img class="bubble left-3" src="<?php bloginfo('template_directory');?>/images/home/icon-flower-2.png" alt="">
      <img class="bubble left-4" src="<?php bloginfo('template_directory');?>/images/home/icon-bubble-1.png" alt="">
      <img class="bubble left-5" src="<?php bloginfo('template_directory');?>/images/home/icon-bubble-3.png" alt="">
      <img class="bubble left-6" src="<?php bloginfo('template_directory');?>/images/home/icon-bubble-3.png" alt="">
      <img class="bubble left-7" src="<?php bloginfo('template_directory');?>/images/home/icon-bubble-3.png" alt="">
      <img class="bubble left-8" src="<?php bloginfo('template_directory');?>/images/home/icon-bubble-3.png" alt="">
      <img class="bubble right-1" src="<?php bloginfo('template_directory');?>/images/home/icon-bubble-2.png" alt="">
      <img class="bubble right-2" src="<?php bloginfo('template_directory');?>/images/home/icon-bubble-1.png" alt="">
      <img class="bubble right-3" src="<?php bloginfo('template_directory');?>/images/home/icon-flower-1.png" alt="">
      <img class="bubble right-4" src="<?php bloginfo('template_directory');?>/images/home/icon-flower-4.png" alt="">
      <img class="bubble right-5" src="<?php bloginfo('template_directory');?>/images/home/icon-bubble-1.png" alt="">
      <img class="bubble right-6" src="<?php bloginfo('template_directory');?>/images/home/icon-b3-1.png" alt="">
      <img class="bubble right-7" src="<?php bloginfo('template_directory');?>/images/home/icon-flower-2.png" alt="">
      <img class="bubble right-8" src="<?php bloginfo('template_directory');?>/images/home/icon-flower-3.png" alt="">
      <img class="bubble right-9" src="<?php bloginfo('template_directory');?>/images/home/icon-b3-1.png" alt="">
      <img class="bubble right-10" src="<?php bloginfo('template_directory');?>/images/home/icon-flower-2.png" alt="">
      <img class="bubble right-11" src="<?php bloginfo('template_directory');?>/images/home/icon-bubble-1.png" alt="">
      <img class="bubble right-12" src="<?php bloginfo('template_directory');?>/images/home/icon-bubble-4.png" alt="">
      <img class="bubble right-13" src="<?php bloginfo('template_directory');?>/images/home/icon-flower-2.png" alt="">
      <img class="bubble right-14" src="<?php bloginfo('template_directory');?>/images/home/icon-b3-2.png" alt="">
      <img class="bubble right-15" src="<?php bloginfo('template_directory');?>/images/home/icon-flower-2.png" alt="">
      <img class="bubble right-16" src="<?php bloginfo('template_directory');?>/images/home/icon-b3-2.png" alt="">
      <img class="bubble right-17" src="<?php bloginfo('template_directory');?>/images/home/icon-flower-2.png" alt="">
      <img class="bubble right-18" src="<?php bloginfo('template_directory');?>/images/home/icon-b3-2.png" alt="">
      <img class="bubble right-19" src="<?php bloginfo('template_directory');?>/images/home/icon-flower-4.png" alt="">
      <img class="bubble right-20" src="<?php bloginfo('template_directory');?>/images/home/icon-bubble-5.png" alt="">
   </div>
</div>

<section id="banner">
   <ul class="banner-list">
      <li>
         <img class="hidden-mb" src="<?php bloginfo('template_directory');?>/images/home/bo-doi-trang-thom.jpg" alt="">
         <img class="hidden-desk" src="<?php bloginfo('template_directory');?>/images/home/bo-doi-trang-thom-mb.jpg" alt="">
      </li>
      <li>
         <img class="hidden-mb" src="<?php bloginfo('template_directory');?>/images/home/banner-1.jpg" alt="">
         <img class="hidden-desk" src="<?php bloginfo('template_directory');?>/images/home/banner-1-mb.jpg" alt="">
      </li>
      <li>
         <img class="hidden-mb" src="<?php bloginfo('template_directory');?>/images/home/banner-2.jpg" alt="">
         <img class="hidden-desk" src="<?php bloginfo('template_directory');?>/images/home/banner-2-mb.jpg" alt="">
      </li>
   </ul>
</section>
<?php 
   $post_type = 'san-pham';
   $taxonomy = 'bo-san-pham';
   $get_terms_args = array(
      'orderby' => 'slug'
   );
   $wp_query_args = array();
   $tax_terms = get_terms( $taxonomy, $get_terms_args );
   if( $tax_terms ){
   ?>
<section id="product">
   <div class="nav-wrap">
      <ul class="cat-nav"> 
         <?php
         foreach( $tax_terms as $key => $tax_term ){
            $query_args = array(
                  'post_type' => $post_type,
                  "$taxonomy" => $tax_term->slug,
                  'post_status' => 'publish',
                  'posts_per_page' => -1,
                  'ignore_sticky_posts' => true
                  );
            $query_args = wp_parse_args( $wp_query_args, $query_args );
            $my_query = new WP_Query( $query_args );
         if( $my_query->have_posts() && $tax_term->slug) { ?>
            <li>
               <a class="<?php if(0 == $key) echo 'active' ?>" href="javascript:;" data-href="#cat_<?php echo $tax_term->slug; ?>"><?php echo $tax_term->name; ?></a>
            </li>
         <?php } }?>
      </ul>
   </div>
   <div class="content-wrap">
      <div class="cat-list">
      <?php
      foreach( $tax_terms as $key => $tax_term ){
         $query_args = array(
            'post_type' => $post_type,
            "$taxonomy" => $tax_term->slug,
            'post_status' => 'publish',
            'posts_per_page' => -1,
            'ignore_sticky_posts' => true
            );
         $query_args = wp_parse_args( $wp_query_args, $query_args );
         $my_query = new WP_Query( $query_args );
         if( $my_query->have_posts() && $tax_term->slug) {?>
         <div id="cat_<?php echo $tax_term->slug; ?>" class="<?php if(0 == $key) echo 'active';?> cat-item">
            <div class="item-list">
               <h2 class="title"><?php echo get_term_meta( $tax_term->term_taxonomy_id, 'wpcf-ten-bo-san-pham', true ); ?></h2>
               <p class="desc"><?php echo get_term_meta( $tax_term->term_taxonomy_id, 'wpcf-mo-ta-bo-san-pham', true ); ?></p>
               <ul class="list-slide">
               <?php while ($my_query->have_posts()) : $my_query->the_post(); ?>
                  <li>
                     <img class="img-product" src="<?php echo get_post_meta( $post->ID, 'wpcf-hinh-san-pham-chung', true ); ?>" alt="">
                     <div class="img-name"><?php echo get_post_meta( $post->ID, 'wpcf-mo-ta-san-pham-chung', true ); ?></div>
                     <a href="javascript:;" data-index="<?php echo $my_query->current_post ?>" class="view-btn">
                        <img src="<?php bloginfo('template_directory');?>/images/home/view-btn.png" alt="">
                     </a>
                  </li>
               <?php endwhile; ?>
               </ul>
            </div>
            
            <div class="item-detail">
               <ul class="detail-slide">
               <?php while ($my_query->have_posts()) : $my_query->the_post(); ?>
                  <li class="slide-item">
                     <div class="img-wrap">
                        <img src="<?php echo get_post_meta( $post->ID, 'wpcf-hinh-san-pham-chi-tiet', true ); ?>" alt="">
                     </div>
                     <div class="content">
                        <div class="p-title">
                           <h2><?php echo str_replace('|', '<br />', get_post_meta( $post->ID, 'wpcf-ten-san-pham', true )); ?></h2>
                           <p><?php echo get_post_meta( $post->ID, 'wpcf-mo-ta-san-pham', true ); ?></p>
                        </div>
                        <div class="p-detail">
                           <?php echo get_post_meta( $post->ID, 'wpcf-chi-tiet-san-pham', true ); ?>
                        </div>
                        <div class="p-footer">
                           <ul class="p-img">
                              <?php
                                 $text_pics = get_post_meta( $post->ID, 'wpcf-text-trong-luong-the-tich',true );
                                 $arr = explode(";", $text_pics);
                                 $img_pics = get_post_meta( $post->ID, 'wpcf-hinh-trong-luong-the-tich' );
                                 foreach ( $img_pics as $key => $image ) {
                                 ?>
                                 <li>
                                    <img src="<?=$image?>" alt="">
                                    <span><?php echo $arr[$key];?></span>
                                 </li>
                              <?php } ?>
                           </ul>
                           <ul class="p-act">
                              <li>
                                 <a class="fb_share_btn" href="javascript:;">
                                    <i class="fa fa-share-alt" aria-hidden="true"></i>
                                 </a>
                              </li>
                              <li>
                                 <a class="ol_cart_btn" href="<?php echo get_post_meta( $post->ID, 'wpcf-link-shop-online', true ); ?>" target="_blank">
                                    <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                                 </a>
                              </li>
                           </ul>
                           <a class="btn-muangay" href="<?php echo get_post_meta( $post->ID, 'wpcf-link-shop-online', true ); ?>" target="_blank">
                              <img src="<?php bloginfo('template_directory');?>/images/home/btn-muangay.png" alt="">
                           </a>
                        </div>
                     </div>
                  </li>
               <?php endwhile; ?>
               </ul>
            </div>
         </div>
      <?php }}?>
      </div>
   </div>
   <?php
      wp_reset_query();
      }
      ?>
</section>

<section id="material">
   <div class="content-wrap">
      <h2 class="title">THÀNH PHẦN</h2>
      <div class="detail">
         <div class="m-img">
            <div class="hidden-desk">
               <img class="bg" src="<?php bloginfo('template_directory');?>/images/home/material-bg-mb.png" alt="">
               <img class="product" src="<?php bloginfo('template_directory');?>/images/home/material-product-mb-2.png" alt="">
               <img class="dot-1" src="<?php bloginfo('template_directory');?>/images/home/material-dot.png" alt="">
               <img class="dot-2" src="<?php bloginfo('template_directory');?>/images/home/material-dot.png" alt="">
               <img class="icon-1" src="<?php bloginfo('template_directory');?>/images/home/material-icon-1.png" alt="">
               <img class="icon-2" src="<?php bloginfo('template_directory');?>/images/home/material-icon-2.png" alt="">
               <img class="icon-3" src="<?php bloginfo('template_directory');?>/images/home/material-icon-3.png" alt="">
               <span class="text-1">Tinh chất<br>sữa dê</span>
               <span class="text-2">Vitamin<br>B3</span>
               <span class="text-3">Hương nước hoa</span>
            </div>
            <div class="hidden-mb">
               <img class="bg" src="<?php bloginfo('template_directory');?>/images/home/material-bg.png" alt="">
               <img class="product" src="<?php bloginfo('template_directory');?>/images/home/material-product.png" alt="">
               <img class="curve" src="<?php bloginfo('template_directory');?>/images/home/material-curve.png" alt="">
               <img class="dot-1" src="<?php bloginfo('template_directory');?>/images/home/material-dot.png" alt="">
               <img class="dot-2" src="<?php bloginfo('template_directory');?>/images/home/material-dot.png" alt="">
               <img class="dot-3" src="<?php bloginfo('template_directory');?>/images/home/material-dot.png" alt="">
               <img class="dot-4" src="<?php bloginfo('template_directory');?>/images/home/material-dot.png" alt="">
               <img class="icon-1" src="<?php bloginfo('template_directory');?>/images/home/material-icon-1.png" alt="">
               <img class="icon-2" src="<?php bloginfo('template_directory');?>/images/home/material-icon-2.png" alt="">
               <img class="icon-3" src="<?php bloginfo('template_directory');?>/images/home/material-icon-3.png" alt="">
            </div>
         </div>
         <div class="m-content">
            <div class="content-mb">
               <div class="c-item">
                  <h2>Tinh chất sữa</h2>
                  <p>
                     Thành phần làm đẹp được kiểm chứng.<br>
                     Hàm lượng axít béo cao cân bằng độ pH, giúp làn da mềm mịn, ẩm và khỏe mạnh.<br>
                     Hàm lượng Vitamin A trong sữa dê cao nuôi dưỡng làn da hồng hào, khỏe mạnh, tránh được các hư tổn và dấu hiệu lão hóa.
                  </p>
               </div>
               <div class="c-item">
                  <h2>Vitamin B</h2>
                  <p>
                     Một "vũ khí" tuyệt vời trong việc làm sáng da và chống lão hóa.<br>
                     Khôi phục lại các tế bào da, sửa chữa các DNA bị hỏng và giảm tác dụng của tia tử ngoại.<br>
                     Vitamin B3 giúp ngăn ngừa lão hóa, giảm các nếp nhăn, làm mờ vết thâm nám và chăm sóc da trở nên sáng mịn đều màu.
                  </p>
               </div>
               <div class="c-item">
                  <h2>Hương nước hoa</h2>
                  <p>
                     Hương hoa lily mang đến những xúc cảm tuyệt vời.
                  </p>
               </div>
            </div>
            <div class="content-desk">
               <div class="desc-1">
                  Hàm lượng axít béo cao cân bằng độ pH,<br>
                  giúp làn da mềm mịn, ẩm và khỏe mạnh.
               </div>
               <div class="desc-2">
                  <h2>Tinh chất sữa dê</h2>
                  <p>
                     Thành phần làm đẹp<br>
                     được kiểm chứng
                  </p>
               </div>
               <div class="desc-3">
                  Hàm lượng Vitamin A trong sữa dê cao<br>
                  nuôi dưỡng làn da hồng hào, khỏe mạnh,<br>
                  tránh được các hư tổn và dấu hiệu lão hóa.
               </div>
               <div class="desc-4">
                  <h2>HƯƠNG NƯỚC HOA</h2>
                  <p>
                     Hương hoa lily mang đến những<br>
                     xúc cảm tuyệt vời
                  </p>
               </div>
               <div class="desc-5">
                  Vitamin B3 giúp ngăn ngừa lão hóa, giảm các<br>
                  nếp nhăn, làm mờ vết thâm nám và chăm sóc<br>
                  da trở nên sáng mịn đều màu.
               </div>
               <div class="desc-6">
                  <h2>VITAMIN B3</h2>
                  <p>
                     Một “vũ khí” tuyệt vời trong<br>
                     việc làm sáng da và chống<br>
                     lão hóa
                  </p>
               </div>
               <div class="desc-7">
                     Khôi phục lại các tế bào da, sửa chữa các<br>
                     DNA bị hỏng và giảm tác dụng của tia tử ngoại.
               </div>
            </div>
         </div>
      </div>
   </div>
</section>

<section id="brand">
   <div class="content-wrap">
      <h2 class="title">KHÁM PHÁ THƯƠNG HIỆU</h2>
      <div class="vid-wrap">
         <?php
         $list_youtube = get_field('video_truyen_thong', 'option');
         $arr = explode(";", $list_youtube);
         ?>
            <a class="vid-item" href="javascript:;" data-url="<?php echo $arr[0]; ?>">
               <img src="https://img.youtube.com/vi/<?php echo  $arr[0];?>/maxresdefault.jpg" alt="">
            </a>
      </div>
      <ul class="vid-list">
      <?php foreach ($arr as $key => $value) {
         if ($key > 0) {
      ?>
         <li>
            <a class="vid-item" href="javascript:;" data-url="<?php echo $value;?>">
               <img src="https://img.youtube.com/vi/<?php echo  $value;?>/maxresdefault.jpg" alt="">
            </a>
         </li>
      <?php }} ?>
      </ul>
   </div>
</section>

<!-- end main -->
   <script type="text/javascript" src="<?php bloginfo('template_directory');?>/js/home.js"></script>
<?php get_footer();?>