$(function () {
   var tlHeader = new TimelineMax();
   tlHeader.to('#header', 1, {
      alpha: 1
   })
   .from('#header .logo', .3, {
      scale: 1.8,
      alpha: 0,
      ease: Back.easeOut
   }, '-=.9')
   .from('.nav-control', .5, {
      opacity: 0
   }, '-=.2')
   .staggerFrom('.main-nav li:not(.socials)', .25, {
      x: 12,
      alpha: 0,
      ease: Power1.easeOut
   }, .1, '-=.9')
   .from('.main-nav .socials', .3, {
      alpha: 0
   })
   .to('section, #bubbles, #footer', 2, {
      alpha: 1
   }, '-=.7')
   
   var winHeight = window.innerHeight;
   var scroll = $(window).scrollTop();

   function checkScroll() {
      if (scroll >= winHeight/2) $('#scrollTop').fadeIn(300);
      else $('#scrollTop').fadeOut(300);

      if (scroll > 15) $('#header').addClass('scroll-active');
      else $('#header').removeClass('scroll-active');
   }

   checkScroll();

   $(window).on('resize', function() {
      winHeight = window.innerHeight;
      scroll = $(window).scrollTop();
      checkScroll();
   });

   $(window).on('scroll', function() {
      scroll = $(window).scrollTop();
      checkScroll();
   });

   $('.nav-control').on('click', function() {
      $('body').toggleClass('nav-active');
   });

   $('#scrollTop').on('click', function() {
      $('html, body').stop().animate({scrollTop : 0}, 800);
      return false;
   });
});