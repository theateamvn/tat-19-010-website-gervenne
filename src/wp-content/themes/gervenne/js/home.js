$(function () {
   $('.banner-list').slick({
      autoplay: true,
      dots: true,
      arrows: false
   });

   $('.vid-list').slick({
      slidesToShow: 3,
      autoplay: true,
      prevArrow: '<button type="button" class="slick-arrow slick-prev"><i class="fa fa-caret-left" aria-hidden="true"></i></button>',
      nextArrow: '<button type="button" class="slick-arrow slick-next"><i class="fa fa-caret-right" aria-hidden="true"></i></button>',
      pauseOnHover: false,
      pauseOnFocus: false
   });

   var tlHome = new TimelineMax();
   tlHome.from('.intro-product', .3, {
      scale: 1.8,
      alpha: 0,
      ease: Back.easeOut
   }, 1)
   .from('.intro-slogan', .3, {
      scale: .2,
      alpha: 0,
      ease: Back.easeOut
   }, '+=.05')
   .from('.intro .intro-btn', .5, {
      autoAlpha: 0
   }, '+=.1')
   .from('.intro-mb .intro-btn', .5, {
      autoAlpha: 0
   }, 1.2);

   var winWidth = window.innerWidth;
   var prevWidth = winWidth;
   var winHeight = window.innerHeight;
   var s2Height = $('#product').offset().top - winHeight/2;
   var s3Height = $('#material').offset().top - winHeight/2;
   var s4Height = $('#brand').offset().top - winHeight/2;
   var s2AnmActive = false;
   var s3AnmActive = false;
   var s4AnmActive = false;
   var scroll = $(window).scrollTop();
   var isDetailSlick = false;

   var listSlideSlickOptions = {
      slidesToShow: 3,
      arrows: true,
      dots: false,
      prevArrow: '<button type="button" class="slick-arrow slick-prev"><i class="fa fa-caret-left" aria-hidden="true"></i></button>',
      nextArrow: '<button type="button" class="slick-arrow slick-next"><i class="fa fa-caret-right" aria-hidden="true"></i></button>'
   }

   var detailSlideSlickOptions = {
      arrows: true,
      dots: false,
      fade: true,
      prevArrow: '<button type="button" class="slick-arrow slick-prev"><i class="fa fa-caret-left" aria-hidden="true"></i></button>',
      nextArrow: '<button type="button" class="slick-arrow slick-next"><i class="fa fa-caret-right" aria-hidden="true"></i></button>'
   }

   if (winWidth >= 768) {
      $('.list-slide').slick(listSlideSlickOptions);
   }

   function checkProductsSlick() {
      if (prevWidth < winWidth && winWidth >= 768) {
         $('.list-slide').slick(listSlideSlickOptions);
      }
      else if (prevWidth > winWidth && winWidth < 768) {
         $('.list-slide').slick('unslick');
      }
   }

   function s2AnmPlay() {
      var tlS2Anm = new TimelineMax();
      tlS2Anm.to('#product', 2, {
         alpha: 1
      });
   }

   function s3AnmPlay() {
      var tlS3Anm = new TimelineMax();
      tlS3Anm.to('#material', 2, {
         alpha: 1
      });
   }

   function s4AnmPlay() {
      var tlS4Anm = new TimelineMax();
      tlS4Anm.to('#brand', 1.5, {
         alpha: 1
      });
   }

   function checkActiveSection() {
      if (scroll >= s4Height) {
         $('.main-nav a').removeClass('active');
         $('.main-nav a[data-href="#brand"]').addClass('active');
      }
      else if (scroll >= s2Height) {
         $('.main-nav a').removeClass('active');
         $('.main-nav a[data-href="#product"]').addClass('active');
      }
      else {
         $('.main-nav a').removeClass('active');
         $('.main-nav a[data-href="#banner"]').addClass('active');
      }
   }

   checkActiveSection();
   checkProductsSlick();
   
   $(window).on('resize', function() {
      prevWidth = winWidth;
      winWidth = window.innerWidth;
      winHeight = window.innerHeight;
      s2Height = $('#product').offset().top - winHeight/2;
      s3Height = $('#material').offset().top - winHeight/2;
      s4Height = $('#brand').offset().top - winHeight/2;
      scroll = $(window).scrollTop();
      homeHeight = $('body').height();
      sceneHome.duration(homeHeight);
      checkActiveSection();
      checkProductsSlick();
   });

   $(window).on('scroll', function() {
      s2Height = $('#product').offset().top - winHeight/2;
      s3Height = $('#material').offset().top - winHeight/2;
      s4Height = $('#brand').offset().top - winHeight/2;
      scroll = $(window).scrollTop();
      checkActiveSection();
      
      if (scroll >= s4Height) {
         if (!s4AnmActive) {
            s4AnmActive = true;
            s4AnmPlay();
         }
      }
      else if (scroll >= s3Height) {
         if (!s3AnmActive) {
            s3AnmActive = true;
            s3AnmPlay();
         }
      }
      else if (scroll >= s2Height) {
         if (!s2AnmActive) {
            s2AnmActive = true;
            s2AnmPlay();
         }
      }
   });

   $('.main-nav a').on('click', function(e) {
      var href = $(this).data('href');
      if (href && $(href).length > 0) {
         e.stopPropagation();
         e.preventDefault();
         animationScroll(href);
      }
   });
   function animationScroll(href) {
      if (href && $(href).length > 0) {
         $('body').removeClass('nav-active');
         if (href == '#product') {
            $('.cat-list .item-detail').hide();
            $('.cat-list .item-list').fadeIn(300);
         }
         if (winWidth >= 1200) {
            $('body, html').stop().animate({
               scrollTop: $(href).offset().top - 55
            }, 700, 'swing');
         }
         else if (winWidth >= 768 && winWidth < 992) {
            $('body, html').stop().animate({
               scrollTop: $(href).offset().top - 47
            }, 700, 'swing');
         }
         else {
            $('body, html').stop().animate({
               scrollTop: $(href).offset().top - 49
            }, 700, 'swing');
         }
      }
   }

   $('.cat-nav').on('click', 'a[data-href]', function() {
      var href = $(this).data('href');
      animationProductScroll(href);
      // set new url
      if ($(href).length > 0 && href != "#banner") {
         var url = window.location.href;
         var url_parth = window.location.pathname;
         var param = url_parth.split('/')[1];
         var href_ = "";
         if(href == '#cat_c-lan-khu-mui'){
            href_ = 'lan-khu-mui';
         } else if(href == '#cat_a-sua-tam'){
            href_ = 'sua-tam';
         } else if(href == '#cat_b-xa-bong-tam'){
            href_ = 'xa-bong-tam';
         }
         if(param){
            window.location = url.replace(param, href_);
         }
         if(href_ && !param){
            window.history.pushState({url: "" + href_ + ""}, '', href_);
         }
      } 
   });
   /**
    * scroll animation by href
    */
   function animationProductScroll(href) {
      if ($(href).length > 0) {
         $('.cat-nav a[data-href]').removeClass('active');
         $('.cat-nav a[data-href="' + href + '"]').addClass('active');
         //$(this).addClass('active');
         $('.cat-list .cat-item, .cat-list .item-detail').hide();
         $(href).addClass('active').fadeIn(300);
         $('.cat-list .item-list').fadeIn(300);
         if (winWidth >= 768) {
            $('.list-slide').slick('unslick');
            $('.list-slide').slick(listSlideSlickOptions);
         }
         else {
            $('body, html').stop().animate({
               scrollTop: $('#product').offset().top - 49
            }, 700, 'swing');
         }
      }
   };
   $('.cat-item').on('click', '.view-btn', function() {
      var index = $(this).data('index');
      var $parent = $(this).parents('.cat-item');
      var $list = $parent.find('.item-list');
      var $detail = $parent.find('.item-detail')
      $list.hide();
      $detail.fadeIn(300);
      if (!isDetailSlick) {
         isDetailSlick = true;
         $('.detail-slide').slick(detailSlideSlickOptions);
         $('.detail-slide').slick('slickGoTo', index);
      }
      else {
         $('.detail-slide').slick('unslick');
         $('.detail-slide').slick(detailSlideSlickOptions);
         $('.detail-slide').slick('slickGoTo', index);
      }

      if (winWidth >= 1200) {
         $('body, html').stop().animate({
            scrollTop: $('#product').offset().top - 55
         }, 700, 'swing');
      }
      else if (winWidth >= 768 && winWidth < 992) {
         $('body, html').stop().animate({
            scrollTop: $('#product').offset().top - 47
         }, 700, 'swing');
      }
      else {
         $('body, html').stop().animate({
            scrollTop: $('#product').offset().top - 49
         }, 700, 'swing');
      }
   });

   $('#brand').on('click', '.vid-item', function() {
      $('#modal_vid .vid-wrap').hide();
      $('body').addClass('ovf-hidden');
      var href = $(this).data('url');
      var vidUrl = 'https://www.youtube.com/embed/' + href + '?rel=0&amp;showinfo=0&amp;modestbranding=1&amp;playsinline=1&amp;autoplay=1&amp;lopp=1&amp;iv_load_policy=3';
      $('#vid_detail').attr('src', vidUrl);
      $('#modal_vid .vid-wrap').show();
      $('#modal_vid').fadeIn(500);
   });

   $('#modal_vid .layer-mask, #modal_vid .close-btn').on('click', function() {
      $('#modal_vid').hide();
      $('body').removeClass('ovf-hidden');
      $('#vid_detail').attr('src', '');
   });

   $('#scrollTop').on('click', function() {
      $('html, body').stop().animate({scrollTop : 0}, 800);
      return false;
   });

   // scrolling animation
   
   
   // init the controller
   var controller = new ScrollMagic.Controller();
   var homeHeight = $('body').height();
   
   // Dealer
   var tlBubbleHome = new TimelineMax({ yoyo: true });
   tlBubbleHome.add([
      TweenMax.to(".home-bubble .left-1", 3, {
         yPercent: -180,
         ease: Power2.easeOut
      }),
      TweenMax.to(".home-bubble .left-2", 3, {
         yPercent: -400,
         ease: Power2.easeOut
      }),
      TweenMax.to(".home-bubble .left-3", 3, {
         yPercent: -800,
         ease: Power2.easeOut
      }),
      TweenMax.to(".home-bubble .left-4", 3, {
         yPercent: -360,
         ease: Power2.easeOut
      }),
      TweenMax.to(".home-bubble .left-5", 3, {
         yPercent: -300,
         ease: Power2.easeOut
      }),
      TweenMax.to(".home-bubble .left-6", 3, {
         yPercent: -400,
         ease: Power2.easeOut
      }),
      TweenMax.to(".home-bubble .left-7", 3, {
         yPercent: -360,
         ease: Power2.easeOut
      }),
      TweenMax.to(".home-bubble .left-8", 3, {
         yPercent: -320,
         ease: Power2.easeOut
      }),
      TweenMax.to(".home-bubble .right-1", 3, {
         yPercent: -380,
         ease: Power2.easeOut
      }),
      TweenMax.to(".home-bubble .right-2", 3, {
         yPercent: -320,
         ease: Power2.easeOut
      }),
      TweenMax.to(".home-bubble .right-3", 3, {
         yPercent: -280,
         ease: Power2.easeOut
      }),
      TweenMax.to(".home-bubble .right-4", 3, {
         yPercent: -300,
         ease: Power2.easeOut
      }),
      TweenMax.to(".home-bubble .right-5", 3, {
         yPercent: -380,
         ease: Power2.easeOut
      }),
      TweenMax.to(".home-bubble .right-6", 3, {
         yPercent: -620,
         ease: Power2.easeOut
      }),
      TweenMax.to(".home-bubble .right-7", 3, {
         yPercent: -880,
         ease: Power2.easeOut
      }),
      TweenMax.to(".home-bubble .right-8", 3, {
         yPercent: -800,
         ease: Power2.easeOut
      }),
      TweenMax.to(".home-bubble .right-9", 3, {
         yPercent: -640,
         ease: Power2.easeOut
      }),
      TweenMax.to(".home-bubble .right-10", 3, {
         yPercent: -780,
         ease: Power2.easeOut
      }),
      TweenMax.to(".home-bubble .right-11", 3, {
         yPercent: -300,
         ease: Power2.easeOut
      }),
      TweenMax.to(".home-bubble .right-12", 3, {
         yPercent: -380,
         ease: Power2.easeOut
      }),
      TweenMax.to(".home-bubble .right-13", 3, {
         yPercent: -980,
         ease: Power2.easeOut
      }),
      TweenMax.to(".home-bubble .right-14", 3, {
         yPercent: -1160,
         ease: Power2.easeOut
      }),
      TweenMax.to(".home-bubble .right-15", 3, {
         yPercent: -940,
         ease: Power2.easeOut
      }),
      TweenMax.to(".home-bubble .right-16", 3, {
         yPercent: -920,
         ease: Power2.easeOut
      }),
      TweenMax.to(".home-bubble .right-17", 3, {
         yPercent: -900,
         ease: Power2.easeOut
      }),
      TweenMax.to(".home-bubble .right-18", 3, {
         yPercent: -300,
         ease: Power2.easeOut
      }),
      TweenMax.to(".home-bubble .right-19", 3, {
         yPercent: -380,
         ease: Power2.easeOut
      }),
      TweenMax.to(".home-bubble .right-20", 3, {
         yPercent: -540,
         ease: Power2.easeOut
      })
   ]);

   // Start animation when scrolling
   var sceneHome = new ScrollMagic.Scene({
      triggerElement: "#product",
      duration: homeHeight,
      triggerHook: "center"
   })
   .setTween(tlBubbleHome)
   .addTo(controller);

   // facebook share
   $('.cat-item').on('click', '.fb_share_btn', function() {
      var url="https://gervenne.vn/"; //Set desired URL here
      var img="https://gervenne.vn/wp-content/themes/gervenne/images/home/banner-1.jpg"; //Set Desired Image here
      var totalurl=encodeURIComponent(url+'?img='+img);

      window.open ('http://www.facebook.com/sharer.php?u='+totalurl,'','width=500, height=500, scrollbars=yes, resizable=no');
   });
   /**
    * link js
    */
      window.onload = function() {
      // your code here
      var url=window.location.pathname;
      var param = url.split('/')[1];
      if ( typeof(param) !== "undefined" && param !== null ) {
         if(param == "xa-bong-tam"){
            $('.cat-list .item-detail').hide();
            $('.cat-list .item-list').fadeIn(300);
            // scroll to 
            animationProductScroll('#cat_b-xa-bong-tam');
            animationScroll('#product');
         } else if (param == "sua-tam") {
            $('.cat-list .item-detail').hide();
            $('.cat-list .item-list').fadeIn(300);
            // scroll to 
            animationScroll('#product');
            animationProductScroll('#cat_a-sua-tam');
         } else if (param == "lan-khu-mui") {
            $('.cat-list .item-detail').hide();
            $('.cat-list .item-list').fadeIn(300);
            // scroll to 
            animationScroll('#product');
            animationProductScroll('#cat_c-lan-khu-mui');
         }
      }
      };
});