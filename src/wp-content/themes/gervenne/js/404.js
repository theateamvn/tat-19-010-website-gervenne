$(function () {
   var tl404 = new TimelineMax();
   tl404.to('#notfound_404', 2, {
      alpha: 1
   }, 1);

   var i = 10;
   var mycd = setInterval(function() {
      i--;
      if (i < 0) clearCD();
      else $('#notfound_404 .cd_404 span').html(i);
      if (i == 0) document.location.href = "/";
   }, 1000);

   function clearCD() {
      clearInterval(mycd);
   }
});