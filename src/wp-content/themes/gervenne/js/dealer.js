$(function () {
   $('body').addClass('is-dealer-page');
   
   // init the controller
   var controller = new ScrollMagic.Controller();
   var dealerHeight = $('#dealer').height();
   
   // Dealer
   var tlBubbleDealer = new TimelineMax({ yoyo: true });
   tlBubbleDealer.add([
      TweenMax.to(".dealer-bubble .left-1", 3, {
         yPercent: -60,
         ease: Power2.easeOut
      }),
      TweenMax.to(".dealer-bubble .left-2", 3, {
         yPercent: -120,
         ease: Power2.easeOut
      }),
      TweenMax.to(".dealer-bubble .left-3", 3, {
         yPercent: -180,
         ease: Power2.easeOut
      }),
      TweenMax.to(".dealer-bubble .left-4", 3, {
         yPercent: -70,
         ease: Power2.easeOut
      }),
      TweenMax.to(".dealer-bubble .right-1", 3, {
         yPercent: -140,
         ease: Power2.easeOut
      }),
      TweenMax.to(".dealer-bubble .right-2", 3, {
         yPercent: -80,
         ease: Power2.easeOut
      }),
      TweenMax.to(".dealer-bubble .right-3", 3, {
         yPercent: -60,
         ease: Power2.easeOut
      }),
      TweenMax.to(".dealer-bubble .right-4", 3, {
         yPercent: -60,
         ease: Power2.easeOut
      }),
      TweenMax.to(".dealer-bubble .right-5", 3, {
         yPercent: -90,
         ease: Power2.easeOut
      })
   ]);
    
   // Start animation when scrolling
   var sceneDealer = new ScrollMagic.Scene({
      triggerElement: "#dealer",
      duration: dealerHeight,
      triggerHook: "onLeave"
   })
   .setTween(tlBubbleDealer)
   .addTo(controller);
   
   $(window).on('resize', function() {
      dealerHeight = $('#dealer').height();
      sceneDealer.duration(dealerHeight);
   });
});